/** BAI TAP 1
 * Input:
 * - So ngay lam viec
 * 
 * Cac buoc xu ly:
 * - B1: gan gia tri n =100 000 (tien luong 1 ngay - co dinh)
 * - B2: gan gia tri a = so ngay lam viec
 * - B3: gan gia tri result = n*days
 * - B4: show up tongLuong
 * 
 * Output:
 * - Tong tien luong
 */
function totalSalary(){
    var daySalary=document.getElementById('daySalary').value*1;
    var workingDays= document.getElementById('workingDays').value*1;
    var totalSalary =daySalary*workingDays;
    document.getElementById('totalSalary').innerHTML=`Bạn nhận được: `
                                                    +totalSalary.toLocaleString()
                                                    +` VND`;
}

/** BAI TAP 2
 * INPUT:
 * - Giá trị số thực 1
 * - Giá trị số thực 2
 * - Giá trị số thực 3
 * - Giá trị số thực 4
 * - Giá trị số thực 5
 * CÁC BƯỚC XỬ LÝ:
 * - Khai báo các biến (chuyển đổi thành dạng số)  num1, num2, num3, num4, num5 và gắn các giá trị được nhập vào từ các ô input (html) có id lần lượt number1, number2, number3, number4, number5.
 * - Khai báo biến result2 = (num1+num2+num3+num4+num5)/5;
 * - Gán giá trị của result2 cho thẻ input #avgValue, hiển thị lên màn hình.
 * OUTPUT:
 * - Giá trị trung bình của 5 số thực được cho tại input
 */
function avgValue(){
    var num1= +document.getElementById('number1').value;
    var num2= +document.getElementById('number2').value;
    var num3= +document.getElementById('number3').value;
    var num4= +document.getElementById('number4').value;
    var num5= +document.getElementById('number5').value;
    var result2=(num1+num2+num3+num4+num5)/5;
    document.getElementById('avgValue').innerHTML=` Giá trị trung bình: ${result2.toLocaleString()}`;
}

/** BAI TAP 3
 * INPUT:
 * - Số tiền USD cần quy đổi (không nhận giá trị âm)
 * CÁC BƯỚC XỬ LÝ:
 * - Khai báo biến cố định n = 23 500;
 * - Khai báo biến usdMoney và gán giá trị bằng giá trị được nhập vào thẻ input có id là usdMoney;
 * - Khai báo biến vndMoney = n*usdMoney;
 * - Gán giá trị của thẻ input #vndMoney = vndMoney và hiển thị lên màn hình;
 * OUTPUT:
 * - Số tiền VND nhận được sau khi quy đổi
 */

function exchangeMoney(){
    var n=23500;
    var usdMoney= +document.getElementById('usdMoney').value;
    var vndMoney= n*usdMoney;
    document.getElementById('vndMoney').innerHTML=`Tương đương: ${vndMoney.toLocaleString()} VND`;
}

/** BAI TAP 4
 * INPUT:
 * - Chiều dài (số dương)
 * - Chiều rộng (số dương)
 * CÁC BƯỚC XỬ LÝ:
 * - Khai báo biến lengthValue và gán giá trị từ thẻ input #lengthValue;
 * - Khai báo biến widthValue và gán giá trị từ thẻ input #widthValue;
 * - Khai báo biến perimeterValue = (lengthValue+widthValue)*2 để tính chu vi;
 * - Khai báo biến areaValue= lengthValue*widthValue để tính diện tích;
 * - Gán giá trị của thẻ input #perimeterValue và input #areaValue lần lượt bằng perimeterValue và areaValue;
 * OUTPUT:
 * - Chu vi
 * - Diện tích
 */

function perimeterValue(){
    var lengthValue = +document.getElementById('lengthValue').value;
    var widthValue = +document.getElementById('widthValue').value;
    var perimeterValue = (lengthValue+widthValue)*2;
    var areaValue= lengthValue*widthValue;
    document.getElementById('perimeterValue').innerHTML=`Chu vi: ${perimeterValue.toLocaleString()}`;
    document.getElementById('areaValue').innerHTML=` Diện tích: ${areaValue.toLocaleString()}`;
}

/** BAI TAP 5
 * INPUT: 
 * - Số có hai chữ số (10 -> 99)
 * CÁC BƯỚC XỬ LÝ:
 * - khai báo biến numberValue và gán giá trị từ thẻ input #numberValue;
 * - khai báo biến donVi=numberValue%10 để chia lấy phần dư;
 * - khai báo biến hangChuc=Math.floor(numberValue/10) để chia lấy phần nguyên;
 * - khai báo biến totalValue=donVi+hangChuc để tính tổng hàng chục và hàng đơn vị;
 * - gán giá trị thẻ input #totalValue = totalValue;
 * OUTPUT:
 * - Tổng hàng đơn vị và hàng chục
 */

function totalValue(){
    var numberValue= + document.getElementById('numberValue').value;
    var donVi=numberValue%10;
    var hangChuc=Math.floor(numberValue/10);
    var totalValue=donVi+hangChuc;
    document.getElementById('totalValue').innerHTML=` Tổng hai ký số: ${totalValue.toLocaleString()}`;
}
    





